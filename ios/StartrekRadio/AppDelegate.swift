//
//  AppDelegate.swift
//  StartrekRadio
//
//  Created by Roman Putintsev on 25.02.2019.
//  Copyright © 2019 Mikro-it. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MPPlayableContentDataSource, MPPlayableContentDelegate {
    var stationNames = ["Retro",
                        "RetroVecherinka",
                        "Retro70",
                        "Retro80",
                        "Retro90",
                        "Rdd",
                        "Keks",
                        "Sanremo",
                        "Eldoradio",
                        "Fresh",
                        "Sportfm",
                        "Europaplus",
                        "Europaplus Top40",
                        "Europaplus Party",
                        "Europaplus Light",
                        "Europaplus RNB",
                        "Europaplus Acoustic",
                        "Europaplus Residance",
                        "Europaplus New",
                        "Europaplus Urban",
                        "Radio7",
                        "Radio7 Love",
                        "Radio7 Happiness",
                        "Radio7 Alone",
                        "Dorognoe",
                        "DorognoeDanceRus",
                        "Dorognoe Rock",
                        "Dorognoe Nostalgia"];
    
    var stationUrls = ["https://hls-01-retro.emgsound.ru/12/playlist.m3u8",
                       "https://hls-01-retro-vecherinka.emgsound.ru/20/playlist.m3u8",
                       "https://hls-01-retro70.emgsound.ru/1/playlist.m3u8",
                       "https://hls-01-retro80.emgsound.ru/2/playlist.m3u8",
                       "https://hls-01-retro90.emgsound.ru/3/playlist.m3u8",
                       "https://hls-01-rdd.emgsound.ru/6/playlist.m3u8",
                       "https://hls-01-keks.emgsound.ru/7/playlist.m3u8",
                       "https://hls-01-sanremo.emgsound.ru/5/playlist.m3u8",
                       "https://hls-01-eldoradio.emgsound.ru/4/playlist.m3u8",
                       "https://hls-01-fresh.emgsound.ru/9/playlist.m3u8",
                       "https://hls-01-sportfm.emgsound.ru/14/playlist.m3u8",
                       "https://hls-02-europaplus.emgsound.ru/11/playlist.m3u8",
                       "https://hls-01-europaplus-top40.emgsound.ru/21/playlist.m3u8",
                       "https://hls-01-europaplus-party.emgsound.ru/22/playlist.m3u8",
                       "https://hls-01-europaplus-light.emgsound.ru/23/playlist.m3u8",
                       "https://hls-01-europaplus-rnb.emgsound.ru/24/playlist.m3u8",
                       "https://hls-01-europaplus-acoustic.emgsound.ru/25/playlist.m3u8",
                       "https://hls-01-europaplus-residance.emgsound.ru/26/playlist.m3u8",
                       "https://hls-01-europaplus-new.emgsound.ru/27/playlist.m3u8",
                       "https://hls-01-europaplus-urban.emgsound.ru/28/playlist.m3u8",
                       "https://hls-01-radio7.emgsound.ru/13/playlist.m3u8",
                       "https://hls-01-radio7-love.emgsound.ru/8/playlist.m3u8",
                       "https://hls-01-radio7-happiness.emgsound.ru/10/playlist.m3u8",
                       "https://hls-01-radio7-alone.emgsound.ru/16/playlist.m3u8",
                       "https://hls-01-dorognoe.emgsound.ru/15/playlist.m3u8",
                       "https://hls-01-dorognoedancerus.emgsound.ru/50/playlist.m3u8",
                       "https://hls-01-dorognoe-rock.emgsound.ru/18/playlist.m3u8",
                       "https://hls-01-dorognoe-nostalgia.emgsound.ru/19/playlist.m3u8"];
    
    var player: STStartrekPlayer? = nil
    var playerDelegate: STStartrekPlayerDelegate = PlayerDelegate.init()
    
    var window: UIWindow?

    func playableContentManager(_ contentManager: MPPlayableContentManager, initiatePlaybackOfContentItemAt indexPath: IndexPath, completionHandler: @escaping (Error?) -> Void) {
        if indexPath.count == 2, indexPath.item < stationNames.count {
            player!.playMasterUrl(stationUrls[indexPath.item])
        }
        completionHandler(nil)
    }
    
    func beginLoadingChildItems(at indexPath: IndexPath, completionHandler: @escaping (Error?) -> Void) {
        completionHandler(nil)
    }
    
    func numberOfChildItems(at indexPath: IndexPath) -> Int {
        if indexPath.indices.count == 0 {
            return 1
        }
        
        return (stationNames.count);
    }
    
    func contentItem(at indexPath: IndexPath) -> MPContentItem? {
        if indexPath.count == 1 {
            let item = MPContentItem(identifier: "Stations")
            item.isContainer = true
            item.isPlayable = false
            item.title = "Stations"
            return item
        } else if indexPath.count == 2, indexPath.item < stationNames.count {
            let item = MPContentItem(identifier: stationNames[indexPath.item])
            item.isContainer = false
            item.isPlayable = true
            item.isStreamingContent = true
            item.title = stationNames[indexPath.item]
            item.subtitle = stationUrls[indexPath.item]
            return item
        } else {
            return nil
        }
    }
    
    @objc func handleInterruption(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSession.InterruptionType(rawValue: typeValue) else {
                return
        }
        if type == .began {
            player!.pause()
        }
        else if type == .ended {
            player!.play()
        }
    }
    
    @objc func handleRouteChange(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSession.RouteChangeReason(rawValue:reasonValue) else {
                return
        }
        if (reason == .oldDeviceUnavailable) {
            player!.pause()
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        MPPlayableContentManager.shared().dataSource = self
        MPPlayableContentManager.shared().delegate = self
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .default)
            try audioSession.setActive(true, options: [])
        } catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(handleInterruption),
                                       name: AVAudioSession.interruptionNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(handleRouteChange),
                                       name: AVAudioSession.routeChangeNotification,
                                       object: nil)
        
        player = STStartrekPlayer.create()

        player!.setDelegate(playerDelegate)
        player!.setPlayingQuality(96000)
        player!.setNetBuffer(30000)
        player!.setNetTimeout(5000)
        player!.setNetReadTimeout(5000)
        player!.setRestarted(true)
        player!.setAgent("BASS AGENT")
        player!.setReferer("C++ test")
        player!.setUserId("InyDrNfj1nCVCmsdfsdfK1ByTSXrSXsPugQfpZj")
        player!.setDaastUrl("http://a.adwolf.ru/2909/getCode?pp=u&ps=clc&p2=jg&rs=image")
        
        let rcc = MPRemoteCommandCenter.shared()
        
        rcc.pauseCommand.isEnabled = true
        rcc.pauseCommand.addTarget { [unowned self] event in
            self.player!.pause()
            return .success
        }
        
        rcc.playCommand.isEnabled = true
        rcc.playCommand.addTarget { [unowned self] event in
            self.player!.play()
            return .success
        }
        
        rcc.likeCommand.isEnabled = false
        rcc.dislikeCommand.isEnabled = false
        rcc.nextTrackCommand.isEnabled = false
        rcc.previousTrackCommand.isEnabled = false
        rcc.togglePlayPauseCommand.isEnabled = false
        rcc.changePlaybackRateCommand.isEnabled = false
        rcc.seekBackwardCommand.isEnabled = false
        rcc.seekForwardCommand.isEnabled = false
        rcc.skipBackwardCommand.isEnabled = false
        rcc.skipForwardCommand.isEnabled = false
        rcc.ratingCommand.isEnabled = false
        rcc.bookmarkCommand.isEnabled = false
        rcc.changePlaybackPositionCommand.isEnabled = false
        rcc.changeRepeatModeCommand.isEnabled = false
        rcc.changeShuffleModeCommand.isEnabled = false
        
        // Init now playing info center
        let songInfo = [
            MPMediaItemPropertyTitle: "Startrek Radio",
            MPMediaItemPropertyArtist: "Player example"
        ]
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo as [String : Any]
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
