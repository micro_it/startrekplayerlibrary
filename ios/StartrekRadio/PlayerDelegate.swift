//
//  PlayerDelegate.swift
//  StartrekRadio
//
//  Created by Roman Putintsev on 26.02.2019.
//  Copyright © 2019 Mikro-it. All rights reserved.
//

import Foundation
import MediaPlayer

class PlayerDelegate: STStartrekPlayerDelegate {
    func onPlay() {
        print("ON PLAY")
    }
    
    func onPause() {
        print("ON PAUSE")
    }
    
    func onStop() {
        print("ON STOP")
    }
    
    func onEnd() {
        print("ON END")
    }
    
    func onStalled() {
        print("ON STALLED")
    }
    
    func onMeta(_ meta: String) {
        print("ON META: \(meta)")
    }
    
    func onError(_ message: String, code: Int32) {
        print("ON ERROR: \(message) CODE \(code)")
    }
    
    func onDebug(_ message: String) {
        print("ON DEBUG: \(message)")
    }
    
    func onDaastStart(_ meta: String, imageUrl: String, clickUrl: String) {
        print("ON DAAST START. Image: \(imageUrl)")
    }
    
    func onDaastEnd() {
        print("ON DAAST END")
    }
    
    func onDaastSkip() {
        print("ON DAAST SKIP")
    }
    
    func onDaastError() {
        print("ON DAAST ERROR")
    }
}
