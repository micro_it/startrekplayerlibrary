//
//  StartrekRadio-Bridging-Header.h
//  StartrekRadio
//
//  Created by Roman Putintsev on 25.02.2019.
//  Copyright © 2019 Mikro-it. All rights reserved.
//

#ifndef StartrekRadio_Bridging_Header_h
#define StartrekRadio_Bridging_Header_h

#import <StartrekPlayer/STStartrekPlayerDelegate.h>
#import <StartrekPlayer/STStartrekPlayer.h>

#endif /* StartrekRadio_Bridging_Header_h */
