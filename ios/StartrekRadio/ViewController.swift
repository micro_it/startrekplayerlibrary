//
//  ViewController.swift
//  StartrekRadio
//
//  Created by Roman Putintsev on 25.02.2019.
//  Copyright © 2019 Mikro-it. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let app = UIApplication.shared.delegate as! AppDelegate
        return (app.stationNames.count);
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let app = UIApplication.shared.delegate as! AppDelegate
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = app.stationNames[indexPath.row]
        
        return (cell)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let app = UIApplication.shared.delegate as! AppDelegate
        let stationUrl = app.stationUrls[indexPath.row]
        let stationName = app.stationNames[indexPath.row]

        let songInfo = [
            MPMediaItemPropertyTitle: stationName,
            MPMediaItemPropertyArtist: stationUrl
        ]
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo as [String : Any]
        
        app.player!.playMasterUrl(stationUrl)
    }
    
    @IBAction func onPlayButton(_ sender: Any) {
        let app = UIApplication.shared.delegate as! AppDelegate
        app.player!.play()
    }
    
    @IBAction func onPauseButton(_ sender: Any) {
        let app = UIApplication.shared.delegate as! AppDelegate
        app.player!.pause()
    }
    
    @IBAction func onStopButton(_ sender: Any) {
        let app = UIApplication.shared.delegate as! AppDelegate
        app.player!.stop()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
