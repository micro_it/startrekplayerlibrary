package org.radiobox.startrek_radio;

import android.media.AudioManager;
import android.util.Log;
import android.media.AudioAttributes;

import org.radiobox.startrek_player.StartrekPlayer;

public class AudioFocusChangeListener implements AudioManager.OnAudioFocusChangeListener
{
    private AudioManager m_audioManager = null;
    private StartrekPlayer m_playback = null;

    public enum FocusState { Granted, Waiting, Abandoned, Ducking }
    private FocusState m_state = FocusState.Abandoned;
    private long focuseLossDate = 0;

    AudioFocusChangeListener(AudioManager audioManager, StartrekPlayer playback) {
        m_audioManager = audioManager;
        m_playback = playback;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        try {
            Log.d("AudioFocusChange", String.valueOf(focusChange));
            switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (m_state == FocusState.Granted) {
                    m_state = FocusState.Ducking;
                    m_playback.setVolume(0.1,0);
                }
                break;
            case AudioManager.AUDIOFOCUS_GAIN:
                if (m_state == FocusState.Waiting) {
                    m_state = FocusState.Granted;
                    if (focuseLossDate != 0) {
                        long milliseconds = System.currentTimeMillis() - focuseLossDate;
                        if (milliseconds < 1000*60*2) { // 2 minutes waiting
                            m_playback.play();
                        }
                        focuseLossDate = 0;
                    }
                } else if (m_state == FocusState.Ducking) {
                    m_state = FocusState.Granted;
                    m_playback.setVolume(1,0);
                }
                break;
            default:
                if (m_state == FocusState.Granted) {
                    m_state = FocusState.Waiting;
                    m_playback.pause();
                    focuseLossDate = System.currentTimeMillis();
                } else if (m_state == FocusState.Ducking) {
                    m_state = FocusState.Waiting;
                    m_playback.pause();
                    m_playback.setVolume(1,0);
                    focuseLossDate = System.currentTimeMillis();
                }
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public FocusState state() {
        return m_state;
    }

    public boolean abandonFocus() {
        try {
            if (m_state == FocusState.Ducking) {
                m_playback.setVolume(1,0);
            }
            if (m_state != FocusState.Waiting) {
                int result = m_audioManager.abandonAudioFocus(this);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    m_state = FocusState.Abandoned;
                }
            }
            Log.d("AudioFocusChange", String.valueOf(m_state));
        } catch (Exception e) {
           e.printStackTrace();
        }
        return m_state == FocusState.Abandoned;
    }

    public boolean grantFocus() {
        try {
            if (m_state == FocusState.Ducking) {
                m_playback.setVolume(1,0);
            }
            if (m_state != FocusState.Granted) {
                int result = m_audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    m_state = FocusState.Granted;
                }
            }
            Log.d("AudioFocusChange", String.valueOf(m_state));
        } catch (Exception e) {
           e.printStackTrace();
        }
        return m_state == FocusState.Granted;
    }
}
