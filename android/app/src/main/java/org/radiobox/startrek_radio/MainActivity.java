package org.radiobox.startrek_radio;

import android.net.Uri;

import androidx.annotation.NonNull;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private MediaBrowserHelper m_mediaBrowserHelper;
    private MediaBrowserCompat.SubscriptionCallback m_mediaBrowserSubscriptionCallback;

    public class OnStationPlayListener implements View.OnClickListener {
        String url;
        public OnStationPlayListener(String url) {
            this.url = url;
        }
        @Override
        public void onClick(View v) {
            try {
                m_mediaBrowserHelper.getTransportControls().playFromUri(Uri.parse(url), new Bundle());
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
    }

    public class ActivitySubscriptionCallback extends MediaBrowserCompat.SubscriptionCallback {
        @Override
        public void onChildrenLoaded(@NonNull String parentId,
                                     @NonNull List<MediaBrowserCompat.MediaItem> children) {
            LinearLayout layStations = (LinearLayout) findViewById(R.id.lay_stations);
            for (MediaBrowserCompat.MediaItem entry : children) {
                Button btnStation = new Button(MainActivity.this);
                btnStation.setText(entry.getMediaId());
                btnStation.setOnClickListener(new OnStationPlayListener(entry.getDescription().getSubtitle().toString()));
                layStations.addView(btnStation);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_mediaBrowserSubscriptionCallback = new ActivitySubscriptionCallback();

        m_mediaBrowserHelper = new MediaBrowserHelper(this, RadioService.class);
        m_mediaBrowserHelper.setSubsriptionCallback(m_mediaBrowserSubscriptionCallback);
        m_mediaBrowserHelper.registerCallback(new MediaBrowserListener());
    }

    public void onClickPlay(View v)
    {
        try {
            m_mediaBrowserHelper.getTransportControls().play();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    public void onClickPause(View v)
    {
        try {
            m_mediaBrowserHelper.getTransportControls().pause();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    public void onClickStop(View v)
    {
        try {
            m_mediaBrowserHelper.getTransportControls().stop();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        m_mediaBrowserHelper.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        m_mediaBrowserHelper.onStop();
    }

    private class MediaBrowserListener extends MediaControllerCompat.Callback {
        @Override
        public void onPlaybackStateChanged(PlaybackStateCompat playbackState) {
            final TextView txt_state = (TextView) findViewById(R.id.txt_state);
            if (playbackState == null) {
                txt_state.setText("PlaybackStateCompat");
            } else {
                txt_state.setText(playbackState.toString());
            }
        }

        @Override
        public void onMetadataChanged(MediaMetadataCompat mediaMetadata) {
            final TextView txt_meta = (TextView) findViewById(R.id.txt_meta);
            if (mediaMetadata == null) {
                txt_meta.setText("MediaMetadataCompat");
            } else {
                txt_meta.setText(mediaMetadata.toString());
            }
        }

        @Override
        public void onSessionDestroyed() {
            super.onSessionDestroyed();
        }

        @Override
        public void onQueueChanged(List<MediaSessionCompat.QueueItem> queue) {
            super.onQueueChanged(queue);
        }
    }
}
