package org.radiobox.startrek_radio;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.support.v4.media.MediaBrowserCompat;
import androidx.media.MediaBrowserServiceCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.ArrayMap;
import android.util.Log;

import org.radiobox.startrek_player.StartrekPlayer;
import org.radiobox.startrek_player.StartrekPlayerDelegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class RadioService extends MediaBrowserServiceCompat {
    static {
        System.loadLibrary("startrek_player");
    }

    private static final String TAG = RadioService.class.getSimpleName();
    private static final String MID_ROOT = "StartrekRoot";

    private static String PACKAGE_NAME;

    private AudioFocusChangeListener m_focusListener;

    private PowerManager.WakeLock m_wakeLock = null;
    private String m_lockTag;

    private PlaybackStateCompat.Builder m_playbackBuilder;
    private MediaMetadataCompat.Builder m_metadataBuilder;
    private MediaSessionCompat m_session;
    private StartrekPlayer m_playback;
    private RadioNotificationManager m_radioNotificationManager;
    private MediaSessionCallback m_callback;
    private boolean m_serviceInStartedState;

    private static final Map<String, String> stationUrls;
    static {
        ArrayMap<String, String> temp = new ArrayMap<>();
        temp.put("Retro", "https://hls-01-retro.emgsound.ru/12/playlist.m3u8");
        temp.put("RetroVecherinka", "https://hls-01-retro-vecherinka.emgsound.ru/20/playlist.m3u8");
        temp.put("Retro70", "https://hls-01-retro70.emgsound.ru/1/playlist.m3u8");
        temp.put("Retro80", "https://hls-01-retro80.emgsound.ru/2/playlist.m3u8");
        temp.put("Retro90", "https://hls-01-retro90.emgsound.ru/3/playlist.m3u8");
        temp.put("Rdd", "https://hls-01-rdd.emgsound.ru/6/playlist.m3u8");
        temp.put("Keks", "https://hls-01-keks.emgsound.ru/7/playlist.m3u8");
        temp.put("Sanremo", "https://hls-01-sanremo.emgsound.ru/5/playlist.m3u8");
        temp.put("Eldoradio", "https://hls-01-eldoradio.emgsound.ru/4/playlist.m3u8");
        temp.put("Fresh", "https://hls-01-fresh.emgsound.ru/9/playlist.m3u8");
        temp.put("Sportfm", "https://hls-01-sportfm.emgsound.ru/14/playlist.m3u8");
        temp.put("Europaplus", "https://hls-02-europaplus.emgsound.ru/11/playlist.m3u8");
        temp.put("Europaplus Top40", "https://hls-01-europaplus-top40.emgsound.ru/21/playlist.m3u8");
        temp.put("Europaplus Party", "https://hls-01-europaplus-party.emgsound.ru/22/playlist.m3u8");
        temp.put("Europaplus Light", "https://hls-01-europaplus-light.emgsound.ru/23/playlist.m3u8");
        temp.put("Europaplus RNB", "https://hls-01-europaplus-rnb.emgsound.ru/24/playlist.m3u8");
        temp.put("Europaplus Acoustic", "https://hls-01-europaplus-acoustic.emgsound.ru/25/playlist.m3u8");
        temp.put("Europaplus Residance", "https://hls-01-europaplus-residance.emgsound.ru/26/playlist.m3u8");
        temp.put("Europaplus New", "https://hls-01-europaplus-new.emgsound.ru/27/playlist.m3u8");
        temp.put("Europaplus Urban", "https://hls-01-europaplus-urban.emgsound.ru/28/playlist.m3u8");
        temp.put("Radio7", "https://hls-01-radio7.emgsound.ru/13/playlist.m3u8");
        temp.put("Radio7 Love", "https://hls-01-radio7-love.emgsound.ru/8/playlist.m3u8");
        temp.put("Radio7 Happiness", "https://hls-01-radio7-happiness.emgsound.ru/10/playlist.m3u8");
        temp.put("Radio7 Alone", "https://hls-01-radio7-alone.emgsound.ru/16/playlist.m3u8");
        temp.put("Dorognoe", "https://hls-01-dorognoe.emgsound.ru/15/playlist.m3u8");
        temp.put("DorognoeDanceRus", "https://hls-01-dorognoedancerus.emgsound.ru/50/playlist.m3u8");
        temp.put("Dorognoe Rock", "https://hls-01-dorognoe-rock.emgsound.ru/18/playlist.m3u8");
        temp.put("Dorognoe Nostalgia", "https://hls-01-dorognoe-nostalgia.emgsound.ru/19/playlist.m3u8");
        stationUrls = Collections.unmodifiableMap(temp);
    }

    private void acquireLock() {
        if (m_wakeLock == null) {
            PowerManager mgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
            if (mgr != null) {
                if (Build.MANUFACTURER.toLowerCase().equals("huawei")) {
                    m_lockTag = "LocationManagerService";
                } else {
                    m_lockTag = PACKAGE_NAME + ":lock";
                }

                m_wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, m_lockTag);

                Log.i(TAG, "Wake lock tag: " + m_lockTag);
            }
        }
        if (m_wakeLock != null && !m_wakeLock.isHeld()) {
            m_wakeLock.acquire();
            Log.i(TAG, "wakeLock " + m_lockTag + " acquired!");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        PACKAGE_NAME = getApplicationContext().getPackageName();

        acquireLock();

        // Create a new MediaSession.
        m_session = new MediaSessionCompat(this, "RadioService");
        m_callback = new MediaSessionCallback();
        m_session.setCallback(m_callback);
        m_session.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        setSessionToken(m_session.getSessionToken());

        // Notification manager
        m_radioNotificationManager = new RadioNotificationManager(this);

        // STPlayer
        m_playback = StartrekPlayer.create();

        m_focusListener = new AudioFocusChangeListener((AudioManager) getSystemService(Context.AUDIO_SERVICE), m_playback);

        m_playback.setDelegate(new MediaPlayerListener());
        m_playback.setNetBuffer(25000);
        m_playback.setNetPrebuf( (int)Math.round(100*(2000.0/25000.0)) ); // 2 sec prebuf
        m_playback.setAgent("Android Startrek Player Radio Service Application");
        m_playback.setRestarted(true);
        m_playback.setReferer("Android Startrek Player"); // utm mark
        m_playback.setUserId("random_CfpM3MCjYHyabaP5"); // userid (should be uniq on device)
        // Alarm test
//        mPlayback.setPrerollUrl("http://a.adwolf.ru/170605/banners/16349/58141_3.mp3");
        m_playback.setDaastUrl("http://a.adwolf.ru/3145/getCode?pp=bl&ps=cmc&p2=jf&plp=f&pli=b&pop=d&dl=http://www.radio7.ru/");
        Log.d(TAG, "onCreate: RadioService creating MediaSession, and RadioNotificationManager");

        // Set an initial metadata
        m_metadataBuilder = new MediaMetadataCompat.Builder();
        Bitmap art = BitmapFactory.decodeResource(getResources(), R.drawable.ic_default_art);
        m_metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, art);
        m_session.setMetadata(m_metadataBuilder.build());

        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
        m_playbackBuilder = new PlaybackStateCompat.Builder();
        m_playbackBuilder.setActions( PlaybackStateCompat.ACTION_PLAY_FROM_URI
                | PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID
                | PlaybackStateCompat.ACTION_PLAY
                | PlaybackStateCompat.ACTION_STOP
                | PlaybackStateCompat.ACTION_PAUSE);

        m_session.setPlaybackState(m_playbackBuilder.build());
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        m_radioNotificationManager.onDestroy();
        m_playback.stop();
        m_playback.deleteDelegate();
        m_playback = null;
        m_focusListener = null;
        m_session.release();
        if (m_wakeLock != null && m_wakeLock.isHeld()) {
            m_wakeLock.release();
        }
        Log.d(TAG, "onDestroy: STPlayer stopped, and MediaSession released");
    }

    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName,
                                 int clientUid,
                                 Bundle rootHints) {
        return new BrowserRoot(
                getString(R.string.app_name), // Name visible in Android Auto
                null); // Bundle of optional extras
    }

    @Override
    public void onLoadChildren(
            @NonNull final String parentMediaId,
            @NonNull final Result<List<MediaBrowserCompat.MediaItem>> result) {
        List<MediaBrowserCompat.MediaItem> ret = new ArrayList<>();

        for (Map.Entry<String, String> entry : stationUrls.entrySet()) {
            MediaDescriptionCompat.Builder bob = new MediaDescriptionCompat.Builder();
            bob.setMediaId(entry.getKey());
            bob.setTitle(entry.getKey());
            bob.setSubtitle(entry.getValue());
//            bob.setDescription(descr);
//            bob.setIconBitmap(icon);
//            bob.setIconUri(iconUri);
            bob.setMediaUri(Uri.parse(entry.getValue()));

            ret.add(new MediaBrowserCompat.MediaItem(
                    bob.build(),
                    MediaBrowserCompat.MediaItem.FLAG_PLAYABLE
            ));
        }

        result.sendResult(ret);
    }

    // MediaSession Callback: Transport Controls -> STPlayer
    public class MediaSessionCallback extends MediaSessionCompat.Callback {
        @Override
        public void onPlayFromUri(Uri uri, Bundle extras) {
            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, uri.toString());
            m_session.setMetadata(m_metadataBuilder.build());

            try {
                m_playback.playMasterUrl(uri.toString());
            } catch(Exception err) {
                err.printStackTrace();
            }
        }

        @Override
        public void onPlayFromMediaId(String mediaId, Bundle extras) {
            String url = stationUrls.get(mediaId);

            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, url);
            m_session.setMetadata(m_metadataBuilder.build());

            try {
                m_playback.playMasterUrl(url);
            } catch(Exception err) {
                err.printStackTrace();
            }
        }

        @Override
        public void onPlayFromSearch(String query, Bundle extras) {
            Log.i(TAG, "On play from search with query: " + query);

            boolean isFound = false;
            for (Map.Entry<String, String> entry : stationUrls.entrySet()) {
                if (entry.getKey().contains(query)) {
                    String url = entry.getValue();
                    m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, url);
                    m_session.setMetadata(m_metadataBuilder.build());

                    try {
                        m_playback.playMasterUrl(url);
                    } catch(Exception err) {
                        err.printStackTrace();
                    }

                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                m_playback.play();
            }
        }

        @Override
        public void onPlay() {
//            mPlayback.playUrl("https://performer.emg.fm/upload/songs/hook/2017/7c/299b/9b5f64659f803bd1.mp3");
            m_playback.play();
        }

        @Override
        public void onPause() {
            m_playback.pause();
        }

        @Override
        public void onStop() {
            m_playback.stop();
        }
    }

    // STPlayer Callback: STPlayer state -> RadioService.
    public class MediaPlayerListener extends StartrekPlayerDelegate {
        MediaPlayerListener() {
            mServiceManager = new ServiceManager();
        }

        @Override
        public void onPlay() {
            m_focusListener.grantFocus();
            if (!m_session.isActive()) {
                m_session.setActive(true);
            }
            m_playbackBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN,
                    1.0f,
                    SystemClock.elapsedRealtime());
            PlaybackStateCompat state = m_playbackBuilder.build();
            m_session.setPlaybackState(state);
            mServiceManager.moveServiceToStartedState(state);
        }

        @Override
        public void onPause() {
            m_playbackBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN,
                    1.0f,
                    SystemClock.elapsedRealtime());
            PlaybackStateCompat state = m_playbackBuilder.build();
            m_session.setPlaybackState(state);
            mServiceManager.updateNotificationForPause(state);
        }

        @Override
        public void onStop() {
            m_focusListener.abandonFocus();
            m_session.setActive(false);
            m_playbackBuilder.setState(PlaybackStateCompat.STATE_STOPPED,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN,
                    1.0f,
                    SystemClock.elapsedRealtime());
            PlaybackStateCompat state = m_playbackBuilder.build();
            m_session.setPlaybackState(state);
            mServiceManager.moveServiceOutOfStartedState(state);
        }

        @Override
        public void onEnd() {
            m_focusListener.abandonFocus();
            Log.i(TAG, "on player end in java");
        }

        @Override
        public void onStalled() {
            m_playbackBuilder.setState(PlaybackStateCompat.STATE_BUFFERING,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN,
                    1.0f,
                    SystemClock.elapsedRealtime());
            m_session.setPlaybackState(m_playbackBuilder.build());
        }

        @Override
        public void onMeta(String meta) {
            m_metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, meta);
            m_session.setMetadata(m_metadataBuilder.build());
            Log.i(TAG, "Metadata: " + meta);
        }

        @Override
        public void onError(String message, int code) {
            m_focusListener.abandonFocus();
            m_session.setActive(false);
            m_playbackBuilder.setState(PlaybackStateCompat.STATE_ERROR,
                    PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN,
                    1.0f,
                    SystemClock.elapsedRealtime());
            m_session.setPlaybackState(m_playbackBuilder.build());
            PlaybackStateCompat state = m_playbackBuilder.build();
            m_session.setPlaybackState(state);
            mServiceManager.moveServiceOutOfStartedState(state);
            Log.e(TAG, message);
        }

        @Override
        public void onDebug(String message) {
            Log.d(TAG, message);
        }

        @Override
        public void onDaastStart(String meta, String imageUrl, String clickUrl) {}

        @Override
        public void onDaastSkip() {}

        @Override
        public void onDaastEnd() {}

        @Override
        public void onDaastError() {}

        private final ServiceManager mServiceManager;

        class ServiceManager {
            private void moveServiceToStartedState(PlaybackStateCompat state) {
                Notification notification =
                        m_radioNotificationManager.getNotification(
                                m_metadataBuilder.build(), state, getSessionToken());

                if (!m_serviceInStartedState) {
                    ContextCompat.startForegroundService(
                            RadioService.this,
                            new Intent(RadioService.this, RadioService.class));
                    m_serviceInStartedState = true;
                }

                startForeground(RadioNotificationManager.NOTIFICATION_ID, notification);
            }

            private void updateNotificationForPause(PlaybackStateCompat state) {
                stopForeground(false);
                Notification notification =
                        m_radioNotificationManager.getNotification(
                                m_metadataBuilder.build(), state, getSessionToken());
                m_radioNotificationManager.getNotificationManager()
                        .notify(RadioNotificationManager.NOTIFICATION_ID, notification);
            }

            private void moveServiceOutOfStartedState(PlaybackStateCompat state) {
                stopForeground(true);
                stopSelf();
                m_serviceInStartedState = false;
            }
        }
    }
}
